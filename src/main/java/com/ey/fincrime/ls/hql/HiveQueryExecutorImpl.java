package com.ey.fincrime.ls.hql;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component("hqlQueryExecutor")
public class HiveQueryExecutorImpl implements HiveQueryExecutor {

	private JdbcTemplate hiveExec;

	public void init(DataSource dataSource) {

		this.hiveExec = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Map<String, Object>> genericResposeBuilder(JSONObject request, DataSource dataSrc) {
		init(dataSrc);
		// TODO Auto-generated method stub
		// hiveExec.execute("select * from fincrime_listscreening_sse.res_iso");
		String qry = null;

		try {
			qry = request.getString("query");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (qry != null && !qry.isEmpty())
			return hiveExec.queryForList(qry);

		return null;
	}

	@Override
	public void genericExecutor(JSONObject request, DataSource dataSrc) {
		init(dataSrc);
		// TODO Auto-generated method stub
		// hiveExec.execute("select * from fincrime_listscreening_sse.res_iso");
		String qry = null;

		try {
			qry = request.getString("query");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (qry != null && !qry.isEmpty())
			hiveExec.execute(qry);

	}

}
