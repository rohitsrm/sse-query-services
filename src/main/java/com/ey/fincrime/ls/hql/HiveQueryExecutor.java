package com.ey.fincrime.ls.hql;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONObject;

public interface HiveQueryExecutor {
	List<Map<String, Object>> genericResposeBuilder(JSONObject request, DataSource dataSrc);

	void genericExecutor(JSONObject request, DataSource dataSrc);
}
