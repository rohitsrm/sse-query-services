package com.ey.fincrime.ls.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.ey.fincrime.ls.dto.IsoCodeDto;

public class HiveServiceSSE {
	public static ArrayList<IsoCodeDto> getISOcode() {
		ArrayList<IsoCodeDto> res = dbConnect(
				"jdbc:hive2://samlp.azurehdinsight.net:443/fincrime_listscreening_sse;transportMode=http;ssl=true;httpPath=/hive2",
				"dev", "Admin2017$");
		return res;

	}

	private static ArrayList<IsoCodeDto> dbConnect(String db_connect_string, String db_userid, String db_password) {
		ResultSet rs = null;
		ArrayList<IsoCodeDto> isoList = new ArrayList<IsoCodeDto>();
		try {
			Class.forName("org.apache.hive.jdbc.HiveDriver");
			Connection conn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
			Statement statement = conn.createStatement();
			String queryString = "select * from fincrime_listscreening_sse.res_iso";
			rs = statement.executeQuery(queryString);
			while (rs.next()) {
				IsoCodeDto isoCode = new IsoCodeDto();
				isoCode.setCountryName(rs.getString(1));
				isoCode.setCountryCode(rs.getString(2));
				System.out.println(isoCode.getCountryCode() + " , " + isoCode.getCountryName());
				isoList.add(isoCode);
			}
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return isoList;
	}

}
