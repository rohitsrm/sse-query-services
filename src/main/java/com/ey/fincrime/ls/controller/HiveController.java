package com.ey.fincrime.ls.controller;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.fincrime.ls.hql.HiveQueryExecutor;

@RestController
public class HiveController {

	@Autowired
	HiveQueryExecutor hqlQueryExecutor;

	@Autowired
	@Qualifier("ssesqlhive")
	DataSource ssesqlhive;

	@PostMapping("/hiveresponse")
	@ResponseBody
	public List<Map<String, Object>> postHiveResponse(@RequestBody String reqStr) {
		System.out.println(reqStr);
		List<Map<String, Object>> response = null;
		JSONObject request = null;
		
		if (reqStr != null && !reqStr.isEmpty()) {
			try {
				request = new JSONObject(reqStr);
			} catch (JSONException e) {
				System.out.println("Error" + e.getMessage());
			}
		} else {

			System.out.println("Error : Request Body is empty");

		}
		
		if (request != null) {
			response = hqlQueryExecutor.genericResposeBuilder(request, ssesqlhive);
		}

		return response;
	}
	
	
}
