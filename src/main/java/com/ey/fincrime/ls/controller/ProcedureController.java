package com.ey.fincrime.ls.controller;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.fincrime.ls.storedproc.SQLServerCallable;

@RestController
public class ProcedureController {
	@Autowired
	SQLServerCallable sqlServerCallable;

	@Autowired
	@Qualifier("lsmastersqlds")
	DataSource lsmastersqlds;

	@Autowired
	@Qualifier("seqasqlds")
	DataSource seqasqlds;

	@Autowired
	@Qualifier("ssesqlds")
	DataSource ssesqlds;

	@Autowired
	@Qualifier("lmsqlds")
	DataSource lmsqlds;

	@Autowired
	@Qualifier("seqac1sqlds")
	DataSource seqac1sqlds;

	@Autowired
	@Qualifier("seqac2sqlds")
	DataSource seqac2sqlds;

	@PostMapping("/callable")
	@ResponseBody
	public Map<String, Object> postCallable(@RequestBody String reqStr) {
		System.out.println(reqStr);
		Map<String, Object> response = new HashMap<String, Object>();
		JSONObject request = null;
		if (reqStr != null && !reqStr.isEmpty()) {
			try {
				request = new JSONObject(reqStr);
			} catch (JSONException e) {
				response.put("Error", e.getMessage());
			}
		} else {

			response.put("Error", "Request Body is empty");

		}
		if (request != null) {
			try {
				if (request.getString("database").endsWith("seqa"))
					response = sqlServerCallable.genericResposeBuilder(request, seqasqlds);
				else if (request.getString("database").endsWith("lsmaster"))
					response = sqlServerCallable.genericResposeBuilder(request, lsmastersqlds);
				else if (request.getString("database").endsWith("listmanagement"))
					response = sqlServerCallable.genericResposeBuilder(request, lmsqlds);
				else if (request.getString("database").endsWith("seqac1"))
					response = sqlServerCallable.genericResposeBuilder(request, seqac1sqlds);
				else if (request.getString("database").endsWith("seqac2"))
					response = sqlServerCallable.genericResposeBuilder(request, seqac2sqlds);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return response;
	}

}
