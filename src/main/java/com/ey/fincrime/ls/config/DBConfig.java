package com.ey.fincrime.ls.config;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DBConfig {

	@Primary
	@Bean(name = "lsmastersqlds")
	@ConfigurationProperties(prefix = "spring.lsmaster.sql.datasource")
	public DataSource lsMasterSQLDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "seqasqlds")
	@ConfigurationProperties(prefix = "spring.seqa.sql.datasource")
	public DataSource seqaSQLDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "ssesqlds")
	@ConfigurationProperties(prefix = "spring.sse.sql.datasource")
	public DataSource sseSQLDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "lmsqlds")
	@ConfigurationProperties(prefix = "spring.lm.sql.datasource")
	public DataSource lmSQLDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "seqac1sqlds")
	@ConfigurationProperties(prefix = "spring.seqac1.sql.datasource")
	public DataSource seqa1SQLDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "seqac2sqlds")
	@ConfigurationProperties(prefix = "spring.seqac2.sql.datasource")
	public DataSource seqa2SQLDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "ssesqlhive")
	@ConfigurationProperties(prefix = "spring.sse.hive.datasource")
	public DataSource sseHiveDataSource() {
		return DataSourceBuilder.create().build();
	}

}
