package com.ey.fincrime.ls.utils;

import java.sql.Date;

public class SQLUtils {

	public static Date convertToSqlDate(java.util.Date utilDate) {
		return new java.sql.Date(utilDate.getTime());
	}

}
