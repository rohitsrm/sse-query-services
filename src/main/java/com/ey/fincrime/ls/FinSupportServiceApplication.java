package com.ey.fincrime.ls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinSupportServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinSupportServiceApplication.class, args);
	}
}
