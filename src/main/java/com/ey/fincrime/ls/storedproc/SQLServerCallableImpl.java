package com.ey.fincrime.ls.storedproc;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

@Component("sqlServerCallable")
public class SQLServerCallableImpl implements SQLServerCallable {

	private SimpleJdbcCall procObj;

	public void init(DataSource dataSource) {

		this.procObj = new SimpleJdbcCall(dataSource);
	}

	@Override
	public Map<String, Object> genericResposeBuilder(org.json.JSONObject request, DataSource dataSrc) {
		init(dataSrc);
		Map resultSet = null;
		try {
			this.procObj.setProcedureName(request.getString("proc_name"));
			this.procObj.setReturnValueRequired(true);
			if (!request.isNull("params")) {
				JSONArray params = request.getJSONArray("params");
				if (params.length() > 0) {
					MapSqlParameterSource inParams = new MapSqlParameterSource();
					for (int i = 0; i < params.length(); i++) {
						JSONObject param = (JSONObject) params.get(i);
						inParams.addValue(param.getString("name"), param.get("value"), param.getInt("type"));
					}

					resultSet = this.procObj.execute(inParams);
				} else {
					resultSet = this.procObj.execute();
				}
			} else {
				resultSet = this.procObj.execute();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultSet;

	}

	private static ArrayList<HashMap<String, String>> resultSetGenericFormat(ResultSet rs) {
		ArrayList<HashMap<String, String>> listOfMaps = new ArrayList<HashMap<String, String>>();

		ResultSetMetaData rsMetaData;
		try {
			rsMetaData = rs.getMetaData();
			while (rs.next()) {
				HashMap map = new HashMap();
				for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
					String key = rsMetaData.getColumnName(i);
					String value = rs.getObject(key).toString();
					map.put(key, value);
				}
				listOfMaps.add(map);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listOfMaps;

	}

}
