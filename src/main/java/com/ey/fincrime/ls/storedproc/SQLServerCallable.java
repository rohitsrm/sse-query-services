package com.ey.fincrime.ls.storedproc;

import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONObject;

public interface SQLServerCallable {

	Map<String, Object> genericResposeBuilder(JSONObject request, DataSource dataSrc);
}
