package com.ey.fincrime.ls.dto;

public class IsoCodeDto {
private String countryName;
private String countryCode;
public String getCountryName() {
	return countryName;
}
public void setCountryName(String countryName) {
	this.countryName = countryName;
}
public String getCountryCode() {
	return countryCode;
}
public void setCountryCode(String countryCode) {
	this.countryCode = countryCode;
}


}
