FROM openjdk:8-jre-alpine

ENV SERVER_PORT 8080

COPY target/fin-support-service-0.0.1-SNAPSHOT.jar /

CMD [ "java", "-jar", "/fin-support-service-0.0.1-SNAPSHOT.jar" ]

EXPOSE $SERVER_PORT